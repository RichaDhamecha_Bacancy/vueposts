import Vue from 'vue'
import Router from 'vue-router'
import Posts from '@/components/Posts'
import Post from '@/components/Post'
import User from '@/components/User'

Vue.use(Router)

export default new Router({
    mode: "history",
    routes: [
        {
            path: '/',
            name: 'Posts',
            component: Posts,  
            template : `<Posts />`
        },
        {
            path: '/post/:id',
            name: 'Post',
            component: Post,  
            template : `<Post />`
        },
        {
            path: '/user/:id',
            name: 'User',
            component: User,  
            template : `<User />`
        },
    ]
})
